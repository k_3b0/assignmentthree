import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-para-display',
  templateUrl: './para-display.component.html',
  styleUrls: ['./para-display.component.css']
})

export class ParaDisplayComponent implements OnInit {
  paragraphToggle = false;
  timeLogs = [];
  date = new Date();

  constructor() {}

  ngOnInit() {}

  btnDisplayDetail() {
    this.paragraphToggle = !this.paragraphToggle;
    this.timeLogs.push(this.paragraphToggle, this.date);
    console.log(this.timeLogs);

    }
}
